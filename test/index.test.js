import DexterCalculations from '../index';
import bigInt from 'big-integer';
import fs from 'fs';

describe('dexter-calculations for xtzToToken', () => {
  it("xtzToTokenTokenOutput", () => {
    const json = JSON.parse(fs.readFileSync('test/xtz_to_token.json'));
    json.map(v => {
      expect(DexterCalculations.xtzToTokenTokenOutput(v['xtz_in'], v['xtz_pool'], v['token_pool'])).toEqual(bigInt(v['token_out']))
      expect(DexterCalculations.xtzToTokenPriceImpact(v['xtz_in'], v['xtz_pool'], v['token_pool'])).toEqual(parseFloat(v['price_impact']));
    });

    expect(DexterCalculations.xtzToTokenTokenOutput(1000000, 1000000000, 250000)).toEqual(bigInt(249));

    // too small
    expect(DexterCalculations.xtzToTokenTokenOutput(5, 100000, 10)).toEqual(bigInt(0));

    expect(DexterCalculations.xtzToTokenTokenOutput(5000, 100000, 10)).toEqual(bigInt(0));

    expect(DexterCalculations.xtzToTokenTokenOutput(10000, 100000, 10)).toEqual(bigInt(0));

    // now enough
    expect(DexterCalculations.xtzToTokenTokenOutput(20000, 100000, 10)).toEqual(bigInt(1));
  });

  it("xtzToTokenXtzInput", () => {
    // based on tzBTC test data
    expect(DexterCalculations.xtzToTokenXtzInput(2000000, 38490742927, 44366268, 8)).toEqual(bigInt(1822514204));

    // based on USDtz test data
    expect(DexterCalculations.xtzToTokenXtzInput(23500000, 3003926688, 667902216, 6)).toEqual(bigInt(109876548));
  });  

  it("xtzToTokenExchangeRate", () => {
    // based on tzBTC test data
    const exchangeRate = DexterCalculations.xtzToTokenExchangeRate(1000000, 34204881343, 39306268);
    expect(exchangeRate).toBeGreaterThan(0.001145 - 0.0005);
    expect(exchangeRate).toBeLessThan(0.001145 + 0.0005);

    // based on USDtz test data
    const exchangeRate2 = DexterCalculations.xtzToTokenExchangeRate(1000000, 3003226688, 668057425);
    expect(exchangeRate2).toBeGreaterThan(0.22170500 - 0.0005);
    expect(exchangeRate2).toBeLessThan(0.22170500 + 0.0005);    
  });

  it("xtzToTokenExchangeRateForDisplay", () => {
    // based on tzBTC test data
    const exchangeRate = DexterCalculations.xtzToTokenExchangeRateForDisplay(1000000, 34204881343, 39306268, 8);
    expect(exchangeRate).toBeGreaterThan(0.00001145 - 0.0005);
    expect(exchangeRate).toBeLessThan(0.00001145 + 0.0005);

    // based on USDtz test data
    const exchangeRate2 = DexterCalculations.xtzToTokenExchangeRateForDisplay(1000000, 3003226688, 668057425, 6);
    expect(exchangeRate2).toBeGreaterThan(0.22170500 - 0.0005);
    expect(exchangeRate2).toBeLessThan(0.22170500 + 0.0005);
    
    const exchangeRate3 = DexterCalculations.xtzToTokenExchangeRateForDisplay(100000, 2000000, 100000, 0);
    expect(exchangeRate3).toEqual(47480.00000000001);
  });

  
  it("xtzToTokenMarketRate", () => {
    expect(DexterCalculations.xtzToTokenMarketRate('1000000','500000000000000000000', '18')).toBe(500.00000000000006);

    expect(DexterCalculations.xtzToTokenMarketRate('144621788919','961208019', '8')).toBe(0.000066463568607795);

    expect(DexterCalculations.xtzToTokenMarketRate('20167031717','41063990114535450000', '18')).toBe(0.0020361940562586686);

    expect(DexterCalculations.xtzToTokenMarketRate('46296642164','110543540642', '6')).toBe(2.3877226398064355);

    expect(DexterCalculations.xtzToTokenMarketRate('58392357794','73989702350', '6')).toBe(1.2671127720347453);
  });  
  
  it("xtzToTokenPriceImpact", () => {
    // good
    const priceImpact0 = DexterCalculations.xtzToTokenPriceImpact(1000000, 29757960047, 351953939);
    expect(priceImpact0).toBe(0.00010314839572795817);

    // too small
    const priceImpact1 = DexterCalculations.xtzToTokenPriceImpact(5, 100000, 10);
    expect(priceImpact1).toBe(0);
    
    const priceImpact2 = DexterCalculations.xtzToTokenPriceImpact(20000, 100000, 10);
    expect(priceImpact2).toBe(0.5);

    const priceImpact3 = DexterCalculations.xtzToTokenPriceImpact(90000, 100000, 10);
    expect(priceImpact3).toBe(0.5555555555555556);

    const priceImpact4 = DexterCalculations.xtzToTokenPriceImpact(200000, 100000, 10);
    expect(priceImpact4).toBe(0.7);
    
    // // based on tzBTC test data    
    const priceImpact5 = DexterCalculations.xtzToTokenPriceImpact(1000000, 34204881343, 39306268);
    expect(priceImpact5).toBe(0.00012362753169542967);

    // // based on tzBTC test data    
    const priceImpact6 = DexterCalculations.xtzToTokenPriceImpact(200000000, 34204881343, 39306268);
    expect(priceImpact6).toBe(0.005814829860627381);
    
    // // based on USDtz test data    
    const priceImpact7 = DexterCalculations.xtzToTokenPriceImpact(10000000, 3003226688, 668057425);
    expect(priceImpact7).toBe(0.003318788783598409);
  });  


  it("xtzToTokenMinimumTokenOutput", () => {
    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(10000, 0.05)).toEqual(bigInt(9500));
    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(10000, 0.01)).toEqual(bigInt(9900));
    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(330000, 0.005)).toEqual(bigInt(328350));

    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(1000, 0.01)).toEqual(bigInt(990));
    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(5000, 0.2)).toEqual(bigInt(4000));
    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(100, 0.055)).toEqual(bigInt(94));

    expect(DexterCalculations.xtzToTokenMinimumTokenOutput(5846941182, 0.3142)).toEqual(bigInt(4009832262));    
  });
  
  it("totalLiquidityProviderFee", () => {
    expect(DexterCalculations.totalLiquidityProviderFee(1000000)).toEqual(bigInt(3000));
    expect(DexterCalculations.totalLiquidityProviderFee(2000000)).toEqual(bigInt(6000));
    expect(DexterCalculations.totalLiquidityProviderFee(1000000000)).toEqual(bigInt(3000000));
    expect(DexterCalculations.totalLiquidityProviderFee(2500000000)).toEqual(bigInt(7500000));
  });

  it("liquidityProviderFee", () => {
    expect(DexterCalculations.liquidityProviderFee(1000000,200000000,100000000)).toEqual(bigInt(1500));
    expect(DexterCalculations.liquidityProviderFee(2000000,200000000,10000000)).toEqual(bigInt(300));    
  });  
});

describe('dexter-calculations for tokenToXtz', () => {
  it("tokenToXtzXtzOutput", () => {
    const json = JSON.parse(fs.readFileSync('test/token_to_xtz.json'));
    json.map(v => {
      expect(DexterCalculations.tokenToXtzXtzOutput(v['token_in'], v['xtz_pool'], v['token_pool'])).toEqual(bigInt(v['xtz_out']));
      expect(DexterCalculations.tokenToXtzPriceImpact(v['token_in'], v['xtz_pool'], v['token_pool'])).toEqual(parseFloat(v['price_impact']));
    });
    return expect(DexterCalculations.tokenToXtzXtzOutput(1000, 20000000, 1000)).toEqual(bigInt(9984977));
  });

  it("tokenToXtzTokenInput", () => {
    // based on tzBTC test data
    expect(DexterCalculations.tokenToXtzTokenInput(12000000, 38490742927, 44366268, 8)).toEqual(bigInt(13877));

    // based on USDtz test data
    expect(DexterCalculations.tokenToXtzTokenInput(1000000, 3003926688, 667902216, 6)).toEqual(bigInt(223086));
  });  

  it("tokenToXtzExchangeRate", () => {
    // based on tzBTC test data
    const exchangeRate = DexterCalculations.tokenToXtzExchangeRate(100000000, 38490742927, 44366268);
    expect(exchangeRate).toBeGreaterThan(266.3723523200   - 0.0005);
    expect(exchangeRate).toBeLessThan(266.3723523200   + 0.0005);

    // based on USDtz test data
    const exchangeRate2 = DexterCalculations.tokenToXtzExchangeRate(34000000, 3003926688, 667902216);
    expect(exchangeRate2).toBeGreaterThan(4.26747503 - 0.0005);
    expect(exchangeRate2).toBeLessThan(4.26747503 + 0.0005);
  });
  
  it("tokenToXtzExchangeRateForDisplay", () => {
    // based on tzBTC test data
    const exchangeRate = DexterCalculations.tokenToXtzExchangeRateForDisplay(100000000, 38490742927, 44366268, 8);
    expect(exchangeRate).toBeGreaterThan(26637.23523200 - 0.0005);
    expect(exchangeRate).toBeLessThan(26637.23523200 + 0.0005);
    
    // based on USDtz test data    
    const exchangeRate2 = DexterCalculations.tokenToXtzExchangeRateForDisplay(34000000, 3003926688, 667902216, 6);
    expect(exchangeRate2).toBeGreaterThan(4.26747503 - 0.0005);
    expect(exchangeRate2).toBeLessThan(4.26747503 + 0.0005);        
  });

  it("tokenToXtzMarketRate", () => {
    expect(DexterCalculations.tokenToXtzMarketRate('1000000','500000000000000000000', '18')).toBe(0.0019999999999999996);

    expect(DexterCalculations.tokenToXtzMarketRate('144621788919','961208019', '8')).toBe(15045.836703428498);

    expect(DexterCalculations.tokenToXtzMarketRate('20167031717','41063990114535450000', '18')).toBe(491.11232641421907);

    expect(DexterCalculations.tokenToXtzMarketRate('46296642164','110543540642', '6')).toBe(0.4188091126367452);

    expect(DexterCalculations.tokenToXtzMarketRate('58392357794','73989702350', '6')).toBe(0.7891957385878037);
    
  });

  it("tokenToXtzPriceImpact", () => {
    // // based on tzBTC test data    
    const priceImpact = DexterCalculations.tokenToXtzPriceImpact(100000000, 3849181242, 44365061);
    expect(priceImpact).toBe(0.6926883784623051);

    // // based on tzBTC test data    
    const priceImpact2 = DexterCalculations.tokenToXtzPriceImpact(40000000, 3849181242, 44365061);
    expect(priceImpact2).toBe(0.47412992462990605);
    
    // // based on USDtz test data    
    const priceImpact3 = DexterCalculations.tokenToXtzPriceImpact(1000000, 2869840667, 699209512);
    expect(priceImpact3).toBe(0.0014283084378495926);

    // // based on USDtz test data    
    const priceImpact4 = DexterCalculations.tokenToXtzPriceImpact(8000000000, 9563874659, 19868860091);
    expect(priceImpact4).toBe(0.28705874495878053);
  });

  
  it("tokenToXtzMinimumTokenOutput", () => {
    expect(DexterCalculations.tokenToXtzMinimumXtzOutput(10000, 0.05)).toEqual(bigInt(9500));
    expect(DexterCalculations.tokenToXtzMinimumXtzOutput(10000, 0.01)).toEqual(bigInt(9900));
    expect(DexterCalculations.tokenToXtzMinimumXtzOutput(330000, 0.005)).toEqual(bigInt(328350));
    expect(DexterCalculations.tokenToXtzMinimumXtzOutput(2739516881, 0.36)).toEqual(bigInt(1753290803));    
  });    
});

describe('dexter-calculations for tokenToToken', () => {
  it("tokenToTokenTokenOutput", () => {
    // trade tzBTC for USDtz
    expect(DexterCalculations.tokenToTokenTokenOutput(200000000, 28044673426, 371387874, 9564874659, 19866789049)).toEqual(bigInt(10037651239));
    expect(DexterCalculations.tokenToTokenTokenOutput(100000000, 28044673426, 371387874, 9564874659, 19866789049)).toEqual(bigInt(7593267334));
    expect(DexterCalculations.tokenToTokenTokenOutput(50000000, 28044673426, 371387874, 9564874659, 19866789049)).toEqual(bigInt(5106286311));
    expect(DexterCalculations.tokenToTokenTokenOutput(40000000, 28044673426, 371387874, 9564874659, 19866789049)).toEqual(bigInt(4387740132));
    expect(DexterCalculations.tokenToTokenTokenOutput(25000000, 28044673426, 371387874, 9564874659, 19866789049)).toEqual(bigInt(3085278056));

    // trade USDtz for tzBTC
    expect(DexterCalculations.tokenToTokenTokenOutput(1000000, 9564874659, 19866789049, 28044673426, 371387874)).toEqual(bigInt(6337));
    expect(DexterCalculations.tokenToTokenTokenOutput(10000000, 9564874659, 19866789049, 28044673426, 371387874)).toEqual(bigInt(63332));
    expect(DexterCalculations.tokenToTokenTokenOutput(15000000000, 9564874659, 19866789049, 28044673426, 371387874)).toEqual(bigInt(47324814));
  });

  it("tokenToTokenTokenInput", () => {
    expect(DexterCalculations.tokenToTokenTokenInput(10037651239, 28044673426, 371387874, 8, 9564874659, 19866789049, 6)).toEqual(bigInt(199999999));
    expect(DexterCalculations.tokenToTokenTokenInput(7593267334, 28044673426, 371387874, 8, 9564874659, 19866789049, 6)).toEqual(bigInt(99999999));
    expect(DexterCalculations.tokenToTokenTokenInput('10000000000000000000', '29738193925', '352189932', '8', '5252041472', '9521487897832635693', '18')).toEqual(null);
  });

  it("tokenToTokenExchangeRate", () => {
    expect(DexterCalculations.tokenToTokenExchangeRate(100000000, 8000000000000, 100000000000, 1000000000000, 2000000000000)).toEqual(157.63079069);
  });

  it("tokenToTokenExchangeRateForDisplay", () => {
    expect(DexterCalculations.tokenToTokenExchangeRateForDisplay(100000000, 8000000000000, 100000000000, 8, 1000000000000, 2000000000000, 6)).toEqual(15763.079069);

    expect(DexterCalculations.tokenToTokenExchangeRateForDisplay(500000, 8000000000000, 100000000000, 8, 1000000000000, 2000000000000, 6)).toEqual(15903.4322);
  });

  it("tokenToTokenMarketRate", () => {
    expect(DexterCalculations.tokenToTokenMarketRate(8000000000000, 100000000000, 8, 1000000000000, 2000000000000, 6)).toEqual(16000);

    expect(DexterCalculations.tokenToTokenMarketRate(236000000, 1000000, 6, 100000000, 4200000000, 8)).toEqual(99.11999999999999);
  });

  it("tokenToTokenPriceImpact", () => {
    // sell one tzBTC for ETHtz
    const priceImpact = DexterCalculations.tokenToTokenPriceImpact(100000000, 200000000000, 1000000000, 11000000000,22000000000000000000);
    expect(priceImpact).toBe(0.3569810253690187);

    // sell 0.025 tzBTC for ETHtz
    const priceImpact2 = DexterCalculations.tokenToTokenPriceImpact(2500000, 200000000000, 1000000000, 11000000000,22000000000000000000);
    expect(priceImpact2).toBe(0.022934153979000203);


    // sell 4 tzBTC for ETHtz
    const priceImpact3 = DexterCalculations.tokenToTokenPriceImpact(400000000, 200000000000, 1000000000, 11000000000,22000000000000000000);
    expect(priceImpact3).toBe(0.5621443546475393);

    // sell 20 wXTZ for USDtz, where 1 XTZ = 1.3 wXTZ
    const priceImpact4 = DexterCalculations.tokenToTokenPriceImpact(20000000, 1000000000, 1300000000, 22000000000, 48000000000);
    expect(priceImpact4).toBe(0.007920219041906122);    
  });  
});


describe('dexter-calculations for addLiquidity', () => {
  it("addLiquidityTokenIn", () => {
    expect(DexterCalculations.addLiquidityTokenIn(1000000, 1000000, 500000)).toEqual(bigInt(500000));
    expect(DexterCalculations.addLiquidityTokenIn(1500000, 1000000, 500000)).toEqual(bigInt(750000));
    expect(DexterCalculations.addLiquidityTokenIn(10000000, 6000000000, 100000000)).toEqual(bigInt(166667));
  });

  it("addLiquidityXtzIn", () => {
    expect(DexterCalculations.addLiquidityXtzIn(1000000, 500000, 1000000)).toEqual(bigInt(500000));
    expect(DexterCalculations.addLiquidityXtzIn(1500000, 500000, 1000000)).toEqual(bigInt(750000));
    expect(DexterCalculations.addLiquidityXtzIn(10000000, 100000000, 6000000000)).toEqual(bigInt(166666));
    
    expect(DexterCalculations.addLiquidityXtzIn(1000000, 1000000, 500000)).toEqual(bigInt(2000000));
    expect(DexterCalculations.addLiquidityXtzIn(1500000, 1000000, 500000)).toEqual(bigInt(3000000));
    expect(DexterCalculations.addLiquidityXtzIn(10000000, 6000000000, 100000000)).toEqual(bigInt(600000000));

    expect(DexterCalculations.addLiquidityXtzIn(1000000, 9563874659, 19868860091)).toEqual(bigInt(481349));    
  });  
});

describe('dexter-calculations for removeLiquidity', () => {
  it("removeLiquidityXtzOut", () => {
    expect(DexterCalculations.removeLiquidityXtzOut(5000000, 100000000, 5000000)).toEqual(bigInt(250000));
    expect(DexterCalculations.removeLiquidityXtzOut(100000000, 100000000, 5000000)).toEqual(bigInt(5000000));
    expect(DexterCalculations.removeLiquidityXtzOut(33333333, 100000000, 5000000)).toEqual(bigInt(1666666));
    expect(DexterCalculations.removeLiquidityXtzOut(2600000, 100000000, 6600000)).toEqual(bigInt(171600));
  });

  it("removeLiquidityTokenOut", () => {
    expect(DexterCalculations.removeLiquidityTokenOut(5000000, 100000000, 5000000)).toEqual(bigInt(250000));
    expect(DexterCalculations.removeLiquidityTokenOut(100000000, 100000000, 5000000)).toEqual(bigInt(5000000));
    expect(DexterCalculations.removeLiquidityTokenOut(33333333, 100000000, 5000000)).toEqual(bigInt(1666666));
    expect(DexterCalculations.removeLiquidityTokenOut(2600000, 100000000, 70000000)).toEqual(bigInt(1820000));
  });  
});
